//Author: Derin Lucio
//c++
#include <iostream>
#include <cmath>
#include <array>
#include <string>
#include <random>
//M2x2
#include "M2x2.h"

std::array<double, 2> conversion(const std::array<double, 2> &target, const M2x2 &key)
{
    return key * target;
}

int main(int argc, char *argv[])
{
    //call mode
    //if mode == 0, encryption
    //if mode == 1, decryption
    int mode = atoi(argv[1]);

    //reading the four requested seeds
    std::array<std::string, 4> seeds;
    for (int i = 0; i < 4; i++)
    {
        seeds[i] = argv[i + 2];
    }

    //initializing PRNG and matrix
    std::default_random_engine r1;
    std::default_random_engine r2;
    std::default_random_engine r3;
    std::default_random_engine r4;
    M2x2 m1;
    M2x2 m2;
    M2x2 m3;
    M2x2 m4;
    //Setting the seeds
    r1.seed(std::stoi(seeds[0]));
    r2.seed(std::stoi(seeds[1]));    
    r3.seed(std::stoi(seeds[2]));    
    r4.seed(std::stoi(seeds[3]));    

    //Setting the four random matrix elements
    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 2; j++)
        {
            m1.set(i, j, ((double)r1()/(double)r1.max()));
            m2.set(i, j, ((double)r2()/(double)r2.max()));
            m3.set(i, j, ((double)r3()/(double)r3.max()));
            m4.set(i, j, ((double)r4()/(double)r4.max()));
        }

    //Producing the keys
    M2x2 key = m1 * m2;
    key = key * m3;
    key = key * m4;

    //reading the target string that is to be encrypted or decrypted
    std::string target = (std::string)argv[6];

    //if in encryption mode
    if (mode == 0)
    {
        //checking if the target string is composed of triplettes of char
        if (target.size() % 3 != 0)
        {
            std::cerr << "Invalid input: string length is not multiple of 3\n";
            return 1;
        }

        std::string encrypted, temp;
        std::string type, a, b;
        for (int i = 0; i < target.size(); i += 3)
        {
            //splitting the string into triplettes of characters
            //the first one is the type (0 if char, 1 if number)
            //the second and the third ones are the actual numbers containing the information
            type = target[i];
            a = target[i + 1];
            b = target[i + 2];
            std::array<double, 2> decrypted = {std::stod(a), std::stod(b)};
            //encryption
            decrypted = conversion(decrypted, key);
            //concatenation of the encrypted string
            //using the character 'n' to delimit the numbers
            temp = type + "n" + std::to_string(decrypted[0]) + "n" + std::to_string(decrypted[1]) + "n";
            encrypted = encrypted + temp;
        }
        //printing the result
        std::cout << encrypted << "\n";
    }

    //if in decryption mode
    if (mode == 1)
    {
        std::string decrypted, temp;
        std::string type;
        int delimiterPosition;
        double a, b;
        //looping until the target string is all read
        //every loop deletes a triplette of characters
        while (target.size() != 0)
        {
            //parsing the input string
            //the delimiter between numbers is the character "n"
            //the first one is the type, the second and the third ones are the numbers to be decrypted
            delimiterPosition = target.find("n");
            type = target.substr(0, delimiterPosition);
            target.erase(0, delimiterPosition + 1);
            delimiterPosition = target.find("n");
            a = std::stod(target.substr(0, delimiterPosition));
            target.erase(0, delimiterPosition + 1);
            delimiterPosition = target.find("n");
            b = std::stod(target.substr(0, delimiterPosition));
            target.erase(0, delimiterPosition + 1);

            std::array<double, 2> encrypted = {a, b};

            //decryption
            encrypted = conversion(encrypted, key.inv());
            //concatenation of the decrypted string
            //rounding the double to the nearest int (to exclude the problem of perfect inversion due to double approximation)
            //casting to int, in order to take the integer part only
            temp = type + std::to_string((int)round(encrypted[0])) + std::to_string((int)round(encrypted[1]));
            decrypted = decrypted + temp;
        }
        std::cout << decrypted << "\n";
    }

    return 0;
}