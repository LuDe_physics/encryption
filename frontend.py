#Author: Derin Lucio
import sys
import os
import subprocess
import json
import random
from getpass import getpass

#dictionary that maps every accepted character (except digits) into a tuple of integers
characters = {
        "a":"01",
        "b":"02",
        "c":"03",
        "d":"04",
        "e":"05",
        "f":"06",
        "g":"07",
        "h":"08",
        "i":"09",
        "j":"10",
        "k":"11",
        "l":"12",
        "m":"13",
        "n":"14",
        "o":"15",
        "p":"16",
        "q":"17",
        "r":"18",
        "s":"19",
        "t":"20",
        "u":"21",
        "v":"22",
        "w":"23",
        "x":"24",
        "y":"25",
        "z":"26",
        "A":"27",
        "B":"28",
        "C":"29",
        "D":"30",
        "E":"31",
        "F":"32",
        "G":"33",
        "H":"34",
        "I":"35",
        "J":"36",
        "K":"37",
        "L":"38",
        "M":"39",
        "N":"40",
        "O":"41",
        "P":"42",
        "Q":"43",
        "R":"44",
        "S":"45",
        "T":"46",
        "U":"47",
        "V":"48",
        "W":"49",
        "X":"50",
        "Y":"51",
        "Z":"52",
        "@":"53",
        "!":"54",
        ".":"55",
        "-":"56",
        "_":"57",
        "&":"58",
        "$":"59",
        "{":"60",
        "}":"61",
        "<":"62",      
        ">":"63",
}

#number of bad attempts allowed before deleting the database
numberOfAllowedBadAttempts = 10

#prints accepted characters
def printChar():
    print ("The characters accepted in DESCRIPTION, USER, PASSWORD, are:")
    print ("- Every letter, uppercase or lowercase (NOT ACCENTED);")
    specialCharacters = "  "
    for i in range (53,(len(characters.items()))):
        specialCharacters = specialCharacters + whatKey(str(i)) + " , "
    specialCharacters = specialCharacters + whatKey(str(len(characters.items())))
    print ("- Special characters: ")
    print (specialCharacters)
    print (separator)
    sys.exit(0)

#finds the key of the dict corresponding to item @a
def whatKey(a):
    for key in list(characters.keys()):
        if characters[key] == a:
            return key
    print ("Unknown error in whatKey.")
    print (separator)
    sys.exit(1)

#converts the string @a into a string of ints, using the "characters" dictionary 
#every character corresponds to a triplette of ints:
#the first one is a bit of state (0 if the char is a symbol, 1 if the char is a digit).
##- If the character is a symbol:
###it is mapped in an int between 00 and 63 using the "characters" dictionary 
###the int corresponding to the character is the second and third digits of the triplette.
##- If the character is a digit:
###the second int is a random int
###the third int is the actual digit.
#the seed is needed to generate always the same random int is the char is a digit, in order to encrypt the string
def toInt(a,seed):
    random.seed(seed)
    stringInInt = ""
    for char in a:
        if char in list(characters.keys()):
            stringInInt = stringInInt + "0" + characters[char]
        else:
            stringInInt = stringInInt + "1" + str(random.randint(0,9)) + char
    return stringInInt

#converts the string of ints @a back into the string of characters
#it follows "toInt()" algorithm backwards
def fromInt(a):
    stringFromInt = ""
    for i in range (0,len(a),3):
        #if it's a character
        if a[i] == "0":
            #get the key corresponding to the int
            stringFromInt = stringFromInt + whatKey(a[i+1:i+3])
        #if it's a number
        elif a[i] == "1":
            stringFromInt = stringFromInt + a[i+2]
        else:
            print ("Unknown error in fromInt.")
            print (separator)
            sys.exit(1)
    return stringFromInt

#function to add a new set of credentials
def addPW():
    #loading existing credentials
    database = {}
    try:
        with open("database.json", "r") as data:
            database = json.load(data)
    except IOError:
        pass
    #getting the encryption key from the user
    while True:
        key = getpass("Insert the encryption key (it must consists of 16 integer digits between 0 and 9): ")
        if len(key) == 16: break
        else:
            print ("Invalid input: key must consists of 16 integer digits between 0 and 9.")

    #parsing the user key into four seeds
    seed1 = key[0:4]
    seed2 = key[4:8]
    seed3 = key[8:12]
    seed4 = key[12:16]

    #geting the credentials from the user
    tempString = "******INSERT THE CREDENTIALS******".center(os.get_terminal_size().columns)
    print (tempString)
    print ("- Description is the name of the credentials. It will be used to get the credentials back.")
    print ("- User and password can't contain some characters: for more info, run the program using 'character_info' argument.")
    description = str(input("Insert the description of the credentials: "))
    print ("IMPORTANT: this description will be asked to get the credentials back.".center(os.get_terminal_size().columns))
    user = str(input("Insert username: "))
    while(True):
        password = str(getpass("Insert password: "))
        passwordCheck = str(getpass("Confirm password: "))
        if(password==passwordCheck):
            break
        else:
            print("Password and confirmation mismatched; try again!")


    #converting data into string of ints
    description = str(toInt(description, seed3))
    user = str(toInt(user,seed3))
    password = str(toInt(password,seed3))

    #encrypting credentials with c++ backend
    #c++ backend converts a string of ints into an encrypted string of doubles and chars, and vice versa
    #encrypting description
    backend = subprocess.Popen(["./backend","0",seed1,seed4,seed2,seed3,description],stdout=subprocess.PIPE)
    (description,error) = backend.communicate()
    description = str(description.rstrip(),'utf-8')
    #encrypting user
    backend = subprocess.Popen(["./backend","0",seed1,seed2,seed3,seed4,user],stdout=subprocess.PIPE)
    (user,error) = backend.communicate()
    user = str(user.rstrip(),'utf-8')
    #encrypting password
    backend = subprocess.Popen(["./backend","0",seed2,seed1,seed4,seed3,password],stdout=subprocess.PIPE)
    (password,error) = backend.communicate()
    password = str(password.rstrip(),'utf-8')
    #updating database.json
    database.update({description:{user:password}})

    #saving the database
    with open("database.json","w") as data:
        json.dump(database,data,indent=4)
    print ("Credentials successfully encrypted and memorized!".center(os.get_terminal_size().columns))

    print (separator)
    sys.exit(0)

#function to get a memorized set of credentials back
def getPW():
    #loading existing credentials
    database = {}

    #bad attempts counter
    numberOfBadAttempts = 0
    #key of the number of bad attempts in the database
    keyNumberBadAttempts = "numberOfBadAttempts"
    #converting the key into a string of ints
    keyNumberBadAttempts = toInt(keyNumberBadAttempts,1928)
    #encrypting the key of the number of bad attempts with arbitrary seed using c++ backend
    #c++ backend converts a string of ints into an encrypted string of doubles and chars, and vice versa
    backend = subprocess.Popen(["./backend","0","1982","2745","1254","9984",keyNumberBadAttempts],stdout=subprocess.PIPE)
    (keyNumberBadAttempts,error) = backend.communicate()
    keyNumberBadAttempts = str(keyNumberBadAttempts.rstrip(),'utf-8')

    try:
        with open("database.json","r") as data:
            database = json.load(data)
    except IOError:
        print ("Error: no credentials memorized!")
        print (separator)
        sys.exit(1)
    
    try:
        numberOfBadAttempts = database[keyNumberBadAttempts]
        #decrypting number of bad attempts
        backend = subprocess.Popen(["./backend","1","1982","2745","1254","9984",numberOfBadAttempts],stdout=subprocess.PIPE)
        (numberOfBadAttempts,error) = backend.communicate()
        numberOfBadAttempts = str(numberOfBadAttempts.rstrip(),'utf-8')
        numberOfBadAttempts = fromInt(numberOfBadAttempts)
        numberOfBadAttempts = int(numberOfBadAttempts)
    except KeyError:
        pass
    
    #getting the encryption key from the user
    while True:
        key = getpass("Insert the encryption key (it must consists of 16 integer digits between 0 and 9): ")
        if len(key) == 16: break
        else:
            print ("Invalid input: key must consists of 16 integer digits between 0 and 9.")

    #parsing the user key in four seeds
    seed1 = key[0:4]
    seed2 = key[4:8]
    seed3 = key[8:12]
    seed4 = key[12:16]

    #getting the credentials from the database
    description = str(input("Insert the description of the credentials used in the memorization: "))
    description = str(toInt(description,seed3))
    #encrypting description with c++ backend
    #c++ backend converts a string of ints into an encrypted string of doubles and chars, and vice versa
    backend = subprocess.Popen(["./backend","0",seed1,seed4,seed2,seed3,description],stdout=subprocess.PIPE)
    (description,error) = backend.communicate()
    description = str(description.rstrip(),'utf-8')

    #reading encrypted credentials
    try:
        credentials = database[description]
    except KeyError:
        print ("No password with such description found: check the key and the description, and try again!".center(os.get_terminal_size().columns))
        numberOfBadAttempts += 1
        print ("Allowed attempts remained: ", numberOfAllowedBadAttempts - numberOfBadAttempts)
        #check if the max number of bad attempts is reached
        #if so, deleting the data
        if numberOfBadAttempts == numberOfAllowedBadAttempts:
            database = {}
            #saving the database
            with open("database.json","w") as data:
                json.dump(database,data,indent=4)       
            print("Max number of bad attempts exceeded: all data erased.")
            print (separator)
            sys.exit(1)

        #updating database with encrypted number of bad attempts (arbitrary seed)
        numberOfBadAttempts = toInt(str(numberOfBadAttempts),9723)
        #encrypting number of bad attempts with c++ backend
        backend = subprocess.Popen(["./backend","0","1982","2745","1254","9984",numberOfBadAttempts],stdout=subprocess.PIPE)
        (numberOfBadAttempts,error) = backend.communicate()
        numberOfBadAttempts = str(numberOfBadAttempts.rstrip(),'utf-8')
        #updating database
        database[keyNumberBadAttempts] = numberOfBadAttempts
        #saving the database
        with open("database.json","w") as data:
            json.dump(database,data,indent=4)
            sys.exit(1)
    user = str(list(credentials.keys())[0])
    password = str(credentials[user])

    #decrypting credentials with c++ backend
    #c++ backend converts a string of ints into an encrypted string of doubles and chars, and vice versa
    #decrypting user
    backend = subprocess.Popen(["./backend","1",seed1,seed2,seed3,seed4,user],stdout=subprocess.PIPE)
    (user,error) = backend.communicate()
    user = str(user.rstrip(),'utf-8')
    #decrypting password
    backend = subprocess.Popen(["./backend","1",seed2,seed1,seed4,seed3,password],stdout=subprocess.PIPE)
    (password,error) = backend.communicate()
    password = str(password.rstrip(),'utf-8')
    #decrypting description
    backend = subprocess.Popen(["./backend","1",seed1,seed4,seed2,seed3,description],stdout=subprocess.PIPE)
    (description,error) = backend.communicate()
    description = str(description.rstrip(),'utf-8')

    #converting the string of ints into normal string
    description = str(fromInt(description))
    user = str(fromInt(user))
    password = str(fromInt(password))

    #printing credentials
    print ("")
    center = round(columns * 0.3)
    descriptionOutput = description.center(center,"*")
    userOutput =  "* USER: "+ user + ";"
    passwordOutput = "* PASSWORD: " + password + ";"
    for i in range (len(userOutput),len(descriptionOutput)-1):
        userOutput = userOutput + " "
    for i in range (len(passwordOutput),len(descriptionOutput)-1):
        passwordOutput = passwordOutput + " "
    userOutput = userOutput + "*"
    passwordOutput = passwordOutput + "*"
    endLine = ""
    for char in descriptionOutput:
        endLine = endLine + "*"
    print (descriptionOutput.center(os.get_terminal_size().columns))
    print (userOutput.center(os.get_terminal_size().columns))
    print (passwordOutput.center(os.get_terminal_size().columns))
    print (endLine.center(os.get_terminal_size().columns))
    print(" ")

    #copying password to the clipboard
    answer = ""
    while(True):
        answer = input("Would you like to copy password to the clipboard? [y/n]:")
        if(answer in ['y','n']):
            if(answer == 'y'):
                command = 'echo "' + password + '"|xclip'
                os.system(command)
                break
            else:
                break


    print (separator)
    
    #reset of the bad attempts counter
    #updating database with encrypted number of bad attempts (arbitrary seed)
    numberOfBadAttempts = 0
    numberOfBadAttempts = toInt(str(numberOfBadAttempts),9723)
    #encrypting number of bad attempts with c++ backend
    backend = subprocess.Popen(["./backend","0","1982","2745","1254","9984",numberOfBadAttempts],stdout=subprocess.PIPE)
    (numberOfBadAttempts,error) = backend.communicate()
    numberOfBadAttempts = str(numberOfBadAttempts.rstrip(),'utf-8')
    #updating database
    database[keyNumberBadAttempts] = numberOfBadAttempts
    #saving the database
    with open("database.json","w") as data:
        json.dump(database,data,indent=4)
    sys.exit(0)

#function to delete a memorized set of credentials
def deletePW():
    #loading existing credentials
    database = {}
    
    #bad attempts counter
    numberOfBadAttempts = 0
    #key of the number of bad attempts in the database
    keyNumberBadAttempts = "numberOfBadAttempts"
    #converting the key into a string of ints
    keyNumberBadAttempts = toInt(keyNumberBadAttempts,1928)
    #encrypting the key of the number of bad attempts with arbitrary seed using c++ backend
    #c++ backend converts a string of ints into an encrypted string of doubles and chars, and vice versa
    backend = subprocess.Popen(["./backend","0","1982","2745","1254","9984",keyNumberBadAttempts],stdout=subprocess.PIPE)
    (keyNumberBadAttempts,error) = backend.communicate()
    keyNumberBadAttempts = str(keyNumberBadAttempts.rstrip(),'utf-8')
    
    try:
        with open("database.json","r") as data:
            database = json.load(data)
    except IOError:
        print ("Error: no credentials memorized!")
        print (separator)
        sys.exit(1)
    
    try:
        numberOfBadAttempts = database[keyNumberBadAttempts]
        #decrypting number of bad attempts
        backend = subprocess.Popen(["./backend","1","1982","2745","1254","9984",numberOfBadAttempts],stdout=subprocess.PIPE)
        (numberOfBadAttempts,error) = backend.communicate()
        numberOfBadAttempts = str(numberOfBadAttempts.rstrip(),'utf-8')
        numberOfBadAttempts = fromInt(numberOfBadAttempts)
        numberOfBadAttempts = int(numberOfBadAttempts)
    except KeyError:
        pass

    #getting the encryption key from the user
    while True:
        key = getpass("Insert the encryption key (it must consists of 16 integer digits between 0 and 9): ")
        if len(key) == 16: break
        else:
            print ("Invalid input: key must consists of 16 integer digits between 0 and 9.")
    
    #parsing the user key in four seeds
    seed1 = key[0:4]
    seed2 = key[4:8]
    seed3 = key[8:12]
    seed4 = key[12:16]

    #getting the description
    description = str(input("Insert the description of the credentials used during the memorization: "))
    #encrypting description with c++ backend
    #c++ backend converts a string of ints into an encrypted string of doubles and chars, and vice versa
    description = str(toInt(description,seed3))
    backend = subprocess.Popen(["./backend","0",seed1,seed4,seed2,seed3,description],stdout=subprocess.PIPE)
    (description,error) = backend.communicate()
    description = str(description.rstrip(),'utf-8')
    #updating the database
    try:
        del database[description]
    except KeyError:
        print ("No password with such description found: check the key and the description, and try again!".center(os.get_terminal_size().columns))
        numberOfBadAttempts += 1
        print ("Allowed attempts remained: ", numberOfAllowedBadAttempts - numberOfBadAttempts)
        #check if the max number of bad attempts is reached
        #if so, deleting the data
        if numberOfBadAttempts == numberOfAllowedBadAttempts:
            database = {}
            #saving the database
            with open("database.json","w") as data:
                json.dump(database,data,indent=4)       
            print("Max number of bad attempts exceeded: all data erased.")
            print (separator)
            sys.exit(1)

        #updating database with encrypted number of bad attempts (arbitrary seed)
        numberOfBadAttempts = toInt(str(numberOfBadAttempts),9723)
        #encrypting number of bad attempts with c++ backend
        backend = subprocess.Popen(["./backend","0","1982","2745","1254","9984",numberOfBadAttempts],stdout=subprocess.PIPE)
        (numberOfBadAttempts,error) = backend.communicate()
        numberOfBadAttempts = str(numberOfBadAttempts.rstrip(),'utf-8')
        #updating database
        database[keyNumberBadAttempts] = numberOfBadAttempts
        #saving the database
        with open("database.json","w") as data:
            json.dump(database,data,indent=4)
            sys.exit(1)
    
    #saving the database
    with open("database.json","w") as data:
        json.dump(database,data,indent=4)

    print ("Credentials successfully deleted!".center(os.get_terminal_size().columns))
    print (separator)
    sys.exit(0)

def listPW():
    #warning print
    print(" ")
    print("!!WARNING!!".center(os.get_terminal_size().columns))
    print("Only 5 attempts to print memorized credentials are allowed.")
    print("After 5 attempts without further access to a specific credential, the database will be erased.")
    print(" ")

    #loading existing credentials
    database = {}
    
    #key of the number of bad attempts in the database
    keyNumberBadAttempts = "numberOfBadAttempts"
    #converting the key into a string of ints
    keyNumberBadAttempts = toInt(keyNumberBadAttempts,1928)
    #encrypting the key of the number of bad attempts with arbitrary seed using c++ backend
    #c++ backend converts a string of ints into an encrypted string of doubles and chars, and vice versa
    backend = subprocess.Popen(["./backend","0","1982","2745","1254","9984",keyNumberBadAttempts],stdout=subprocess.PIPE)
    (keyNumberBadAttempts,error) = backend.communicate()
    keyNumberBadAttempts = str(keyNumberBadAttempts.rstrip(),'utf-8')

    try:
        with open("database.json","r") as data:
            database = json.load(data)
    except IOError:
        print ("Error: no credentials memorized!")
        print (separator)
        sys.exit(1)
    
    try:
        numberOfBadAttempts = database[keyNumberBadAttempts]
        #decrypting number of bad attempts
        backend = subprocess.Popen(["./backend","1","1982","2745","1254","9984",numberOfBadAttempts],stdout=subprocess.PIPE)
        (numberOfBadAttempts,error) = backend.communicate()
        numberOfBadAttempts = str(numberOfBadAttempts.rstrip(),'utf-8')
        numberOfBadAttempts = fromInt(numberOfBadAttempts)
        numberOfBadAttempts = int(numberOfBadAttempts)
    except KeyError:
        pass

    #getting the encryption key from the user
    while True:
        key = getpass("Insert the encryption key (it must consists of 16 integer digits between 0 and 9): ")
        if len(key) == 16: break
        else:
            print ("Invalid input: key must consists of 16 integer digits between 0 and 9.")

    #parsing the user key in four seeds
    seed1 = key[0:4]
    seed2 = key[4:8]
    seed3 = key[8:12]
    seed4 = key[12:16]

    #title print
    print ("")
    center = round(columns * 0.3)
    title = "Memorized Credentials"
    titleOutput = title.center(center,"*")
    print (titleOutput.center(os.get_terminal_size().columns))

    #array for the decrypted descriptions
    descriptions = []
    counter = 0
    descriptionOutput = ""
    for description in database:
        if description == keyNumberBadAttempts:
            continue

        #decrypting description
        backend = subprocess.Popen(["./backend","1",seed1,seed4,seed2,seed3,description],stdout=subprocess.PIPE)
        (description,error) = backend.communicate()
        description = str(description.rstrip(),'utf-8')

        #converting the string of ints into normal string
        description = str(fromInt(description))
        counter += 1
        descriptionOutput =  "* - " + str(counter) + ": "+ description + ";"
        for i in range (len(descriptionOutput),len(titleOutput)-1):
            descriptionOutput = descriptionOutput + " "
        descriptionOutput = descriptionOutput + "*"

        print (descriptionOutput.center(os.get_terminal_size().columns))
    
    endLine = ""
    for char in descriptionOutput:
        endLine = endLine + "*"
    print (endLine.center(os.get_terminal_size().columns))
    
    numberOfBadAttempts += 2
    print ("IMPORTANT: allowed attempts to print all credentials remained: ", int((numberOfAllowedBadAttempts - numberOfBadAttempts)/2))
    #check if the max number of bad attempts is reached
    #if so, deleting the data
    if numberOfBadAttempts == numberOfAllowedBadAttempts:
        database = {}
        #saving the database
        with open("database.json","w") as data:
            json.dump(database,data,indent=4)       
        print("Max number of bad attempts exceeded: all data erased.")
        print (separator)
        sys.exit(1)

    #updating database with encrypted number of bad attempts (arbitrary seed)
    numberOfBadAttempts = toInt(str(numberOfBadAttempts),9723)
    #encrypting number of bad attempts with c++ backend
    backend = subprocess.Popen(["./backend","0","1982","2745","1254","9984",numberOfBadAttempts],stdout=subprocess.PIPE)
    (numberOfBadAttempts,error) = backend.communicate()
    numberOfBadAttempts = str(numberOfBadAttempts.rstrip(),'utf-8')
    #updating database
    database[keyNumberBadAttempts] = numberOfBadAttempts
    #saving the database
    with open("database.json","w") as data:
        json.dump(database,data,indent=4)

    print (separator)
    sys.exit(0)

#main
if __name__ == "__main__":
    #call mode
    modes = ['addPW','getPW','deletePW','listPW','character_info']
    #mode=="addPW": add a new set of credentials
    #mode=="getPW": print an existing set of credentials
    #mode=="deletePW": delete an existing set of credentials
    #mode=="character_info": print all accepted characters
    
    columns = os.get_terminal_size().columns
    separator = ""
    for i in range (0,columns):
        separator = separator + "-"
    beginString = "ENCRYPTED CREDENTALS DATABASE MANAGER".center(len(separator),"-")
    print(beginString)

    try:
        mode = sys.argv[1]
    except IndexError:
        print ("INVALID ARGUMENTS!".center(os.get_terminal_size().columns))
        print ("Accepted arguments are:")
        print ("- addPW: add a new set of credentials;")
        print ("- getPW: get a memorized set of credentials;")
        print ("- deletePW: delete a memorized set of credentials;")
        print ("- character_info: get the list of accepted character.")
        print (separator)
        sys.exit(1)
    if mode not in modes:
        print ("INVALID ARGUMENTS!".center(os.get_terminal_size().columns))
        print ("Accepted arguments are:")
        print ("- addPW: add a new set of credentials;")
        print ("- getPW: get a memorized set of credentials;")
        print ("- deletePW: delete a memorized set of credentials;")
        print ("- character_info: get the list of accepted character.")
        print (separator)
        sys.exit(1)

    if mode == "addPW":
        addPW()
    elif mode == "getPW":
        getPW()
    elif mode == "deletePW":
        deletePW()
    elif mode == "listPW":
        listPW()
    elif mode == "character_info":
        printChar()
    else:
        print("Error: Unknown State in mode selection.")
        sys.exit(1)