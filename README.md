# Database manager of encrypted credentials, composed of a python front-end and a c++ back-end

## **Features**
- [x] Memorization of a new set of credentials (description, user and password) in the database;
- [x] Printing of a memorized set of credentials;
- [x] Deletion of a memorized set of credentials;
- [x] Printing of the accepted characters;
- [x] Deletion of all the credentials after a certain number of bad attempts.
- [x] Printing all the descriptions of memorized credentials.
- [x] Copy passwords to the clipboard.
- [x] Double check the password when memorizing a new set of credentials.

## **Upcoming Features**
- No upcoming features yet.

## **Prerequisites**
- To copy the password to the clipboard, this program makes use of ```xclip```. You can install it with:
    ```
    $sudo apt-get install xclip
    ```
  If you are using WSL (Windows Subsystem for Linux) you need to run an Xserver to be able to use xclip.

## **Instructions**
- Compile the c++ backend with the Makefile
    ```
    $ make
    ```


- Every set of credentials is encrypted with a 16 integers digits key;

- To memorize a new set of credentials:
    - run the front-end with python3 and "addPW" as argument (without the inverted commas);
        ```
        $ python3 frontend.py addPW
        ```
    - insert the encryption key;
    - insert a description for the set of credentials (it will be used to get the credentials back);
    - insert user and password;

- To get a memorized set of credentials back:
    - run the front-end with python3 and "getPW" as argument (without the inverted commas);
        ```
        $ python3 frontend.py getPW
        ```
    - insert the encryption key set during the memorization;
    - insert the description set during the memorization;

- To delete a memorized set of credentials:
    - run the front-end with python3 and "deletePW" as argument (without the inverted commas);
        ```
        $ python3 frontend.py deletePW
        ```
    - insert the encryption key;
    - insert the description of the credential to be deleted;

- To print all memorized sets of credentials' descriptions:
    - run the front-end with python3 and "listPW" as argument (without the inverted commas);
        ```
        $ python3 frontend.py listPW
        ```
    - insert the encryption key;
    - If the key is correct, the memorized sets of credentials' descriptions will be shown.
    - If the key is incorrect, either the printed description is nonsense or errors from internal functions occur.

- To print information about all the accepted characters:
    - run the front-end with python3 and "character_info" as argument (without the inverted commas);
        ```
        $ python3 frontend.py character_info
        ```

- Number of allowed bad attempts:
    - The maximum number of allowed bad attempts is 10;
    - If the user tries to get back a memorized set of credentials or to delete it, but the 
    program can not match the description given in input and one of the memorized descriptions (this can occur because the description or the key given in input is wrong), a bad attempt is counted;
    - If the user prints the description of all the memorized sets of credentials, two bad attempts are counted: this is because printing all the descriptions does not require a match between the user description and the memorized ones, hence the program can not tell if the input key is correct. If the bad attempts counter was not increased it would be possible to use this function to test different keys limitless, until a useful set of descriptions is printed, leading to a potential way to find the encryption key. This is avoided by increasing of 2 the bad attempts counter after each call.
    - Every time a memorized set of credentials is successfully printed using "getPW" or is deleted using "deletePW", the counter of bad attempts gets reset. This is because the user needs to know the right key to use "getPW" and "deletePW".
    - When the maximum number of bad attempts is reached, the database is erased.

## Working algorithm

- Every string of characters (description, user and password) is converted into a string of integers in the following way:
    - Every character is converted into a triplette of integers:
        - the first integer is a bit of state: 0 if the character is a symbol, 1 if the character is a number;
        - if the character is a symbol, it is related to an integer between 00 and 63; this integer is concatenated to the bit of state, forming the triplette of integers;
        - if the character is a digit, the second integer of the triplette is a random integer (seed taken from the encryption key), and the third is the actual digit;
- The c++ back-end converts strings of integers into strings of doubles, and vice versa:
    - the encryption key is split into four strings composed of four digits;
    - the strings are used as seeds for four pseudo random number generator;
    - the four PRNGs are used to fill four 2x2 matrices of doubles;
    - the matrices are used to convert the tuple of integers representing a character into a tuple of doubles (the bit of state is left unchanged);
    - the decryption is done by inverting the matrices;
    - description, user and password are encrypted with a permutation of the seeds extracted from the user's encryption key: this way, the actual key is different for each of them;
- The encrypted credentials are memorized in "database.json"


## Feel free to report bug or suggestions!
*Please note that this is an amateur project: neither it's meant to be as secure as a professional project, nor it's meant to protect highly sensible data. Moreover, the project is still in testing phase.*
