//Author: Derin Lucio
//**************************************//
//*************M2x2 class**************//
//Class that manages 2x2 matrix of real numbers
//***********CONSTRUCTOR**************//
//M2x2(double a = 0, double b = 0, double c = 0, double d = 0)
//*************GETTER****************//
//double M2x2::at(int i, int j): returns the (i,j) element
//*************SETTER****************//
//void M2x2::set(int i, int j, double e): sets @e as the (i,j) element
//************OPERATORS*************//
//M2x2 M2x2::operator*(double): product by scalar
//M2x2 M2x2::operator*(M2x2): inner product
//std::array<double,2> M2x2::operator*(std::array<double,2>):product by vector
//*************METHODS****************//
//M2x2 inv(): returns the inverse matrix
//double det(): returns the determinant
//void print(): prints the matrix on the terminal
//**************************************//

#ifndef _MATRIX
#define _MATRIX
//c++
#include <iostream>
#include <cmath>
#include <array>

class M2x2{
private:
    std::array<std::array<double, 2>, 2> mat;

public:
    //constructor
    M2x2(double a = 0, double b = 0, double c = 0, double d = 0) : mat({a, b, c, d}) {}
    
    //SETTERS & GETTERS
    //getters
    double at(int, int) const;
    //setters
    void set(int, int, double);

    //OPERATORS
    //product by scalar
    void operator*(double);
    //inner product
    M2x2 operator*(const M2x2) const;
    //product by vector
    std::array<double,2> operator*(const std::array<double,2>) const;
    
    //METHODS
    //inverse
    M2x2 inv() const;
    //determinant
    double det() const;
    //terminal print
    void print() const;

};
#endif //_MATRIX