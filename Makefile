all: main

main: backend.o M2x2.o
	g++ -g -o backend backend.cpp M2x2.o
backend.o: backend.cpp
	g++ -c backend.cpp
M2x2.o: M2x2.h M2x2.cpp
	g++ -c M2x2.cpp
clean:
	rm backend backend.o M2x2.o
databaseClean:
	rm database.json
