//Author: Derin Lucio
#include "M2x2.h"

//SETTERS & GETTERS
//getter
double M2x2::at(int i, int j) const { return mat[i][j]; }

//setter
void M2x2::set(int i, int j, double a) { mat[i][j] = a; }

//OPERATORS
//product by scalar
void M2x2::operator*(double a)
{
    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 2; j++)
            mat[i][j] = a * mat[i][j];
}

//inner product
M2x2 M2x2::operator* (const M2x2 a) const
{
    M2x2 temp(0, 0, 0, 0);
    temp.set(0, 0, (mat[0][0] * a.at(0, 0)) + (mat[0][1] * a.at(1, 0)));
    temp.set(0, 1, (mat[0][0] * a.at(0, 1)) + (mat[0][1] * a.at(1, 1)));
    temp.set(1, 0, (mat[1][0] * a.at(0, 0)) + (mat[1][1] * a.at(1, 0)));
    temp.set(1, 1, (mat[1][0] * a.at(0, 1)) + (mat[1][1] * a.at(1, 1)));
    return temp;
}

//product by vector
std::array<double, 2> M2x2::operator*(const std::array<double, 2> a) const
{
    std::array<double, 2> temp;
    temp[0] = (mat[0][0] * a[0]) + (mat[0][1] * a[1]);
    temp[1] = (mat[1][0] * a[0]) + (mat[1][1] * a[1]);
    return temp;
}

//METHODS
//inverse
M2x2 M2x2::inv() const
{
    M2x2 temp(mat[1][1], -mat[0][1], -mat[1][0], mat[0][0]);
    temp *std::pow(det(), -1);
    return temp;
}

//determinant
double M2x2::det() const { return ((mat[0][0] * mat[1][1]) - (mat[0][1] * mat[1][0])); }

//terminal print
void M2x2::print() const
{
    std::cout << mat[0][0] << " " << mat[0][1] << "\n";
    std::cout << mat[1][0] << " " << mat[1][1] << "\n";
}